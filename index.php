<?php 
class a{
    public $i = 10;
    public $p = 20;

    function add(){
        return $this->i + $this->p;
    }

    function mul($a=1, $b=1){
        echo $a * $b;
    }
}

$obj = new a();
// echo $obj->p + $obj->i;

$obj->i = 40;
echo $obj->add();

echo $obj->mul();

?>
